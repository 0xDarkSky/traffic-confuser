#!/usr/bin/python3
#your python location ^

try:
   import os, math, time, sys, random, requests as req
   import os.path
   from os import path
   from termcolor import colored
except ModuleNotFoundError:
   print("Some modules might not be installed (termcolor, requests). Would you like to install them? Y/n")
   modules = input("")
   if modules == "y":
      print("Installing modules...")
      os.system("pip install termcolor")
      os.system("pip install requests")
      print("Run the program again.")
      exit()
   else:
      print("Exitting.")
      exit()

logo = """
░██████╗░██╗░░░░░░█████╗░██████╗░  ████████╗██████╗░░█████╗░███████╗███████╗██╗░█████╗░
██╔═══██╗██║░░░░░██╔══██╗██╔══██╗  ╚══██╔══╝██╔══██╗██╔══██╗██╔════╝██╔════╝██║██╔══██╗
██║██╗██║██║░░░░░██║░░██║██████╔╝  ░░░██║░░░██████╔╝███████║█████╗░░█████╗░░██║██║░░╚═╝
╚██████╔╝██║░░░░░██║░░██║██╔══██╗  ░░░██║░░░██╔══██╗██╔══██║██╔══╝░░██╔══╝░░██║██║░░██╗
░╚═██╔═╝░███████╗╚█████╔╝██║░░██║  ░░░██║░░░██║░░██║██║░░██║██║░░░░░██║░░░░░██║╚█████╔╝
░░░╚═╝░░░╚══════╝░╚════╝░╚═╝░░╚═╝  ░░░╚═╝░░░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░░░░╚═╝░░░░░╚═╝░╚════╝░

         ░█████╗░░█████╗░███╗░░██╗███████╗██╗░░░██╗░██████╗███████╗██████╗░
         ██╔══██╗██╔══██╗████╗░██║██╔════╝██║░░░██║██╔════╝██╔════╝██╔══██╗
         ██║░░╚═╝██║░░██║██╔██╗██║█████╗░░██║░░░██║╚█████╗░█████╗░░██████╔╝
         ██║░░██╗██║░░██║██║╚████║██╔══╝░░██║░░░██║░╚═══██╗██╔══╝░░██╔══██╗
         ╚█████╔╝╚█████╔╝██║░╚███║██║░░░░░╚██████╔╝██████╔╝███████╗██║░░██║
         ░╚════╝░░╚════╝░╚═╝░░╚══╝╚═╝░░░░░░╚═════╝░╚═════╝░╚══════╝╚═╝░░╚═╝
"""

os.system('cls' if os.name == 'nt' else 'clear')

headers = {"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4369.0 Safari/537.36"}

def super_type(b):
    for a in b:
        sys.stdout.write(a)
        sys.stdout.flush()
        time.sleep(0.03)

def web_check():
    if path.exists("sample.txt"):
       pass
    else:
       super_type("File sample.txt is missing. Creating file...")
       Q = open("sample.txt", "a")
       Q.write("https://whatsapp.com\nhttps://vimeo.com\nhttps://mozilla.org\nhttps://linkedin.com\nhttps://microsoft.com\nhttps://www.blogger.com\nhttps://apple.com\nhttps://www.reddit.com\nhttps://www.github.com\nhttps://www.youtube.com\nhttps://www.facebook.com\nhttps://www.google.com\nhttps://www.weather.com\nhttps://www.cnn.com\nhttps://www.twitter.com\nhttps://www.wikipedia.org\nhttps://www.quora.com\nhttps://duckduckgo.com\n")
       Q.close()
       super_type("\n\nFile created.\n")
       
try:
   usr_input = sys.argv[1]
except IndexError:
   super_type("Wrong input!\nUsage: specify slow or fast, ex.: python3 traffic-confuser.py slow\n")
   exit()

def start_status():
    print(colored(f"{logo}", "magenta"))
    super_type(colored("Started...\n\n", "yellow"))

def arg():
   try:
      if usr_input.lower() == "slow":
         sleeper = random.randint(85,600)
         sleeps = float(sleeper)
         time.sleep(sleeps)
      if usr_input.lower() == "fast":
         sleeper = random.randint(15,35)
         sleeps = int(sleeper)
         time.sleep(sleeps)
   except:
      pass

def text_stuff():
       with open("sample.txt") as f:
            try:
               INFO = colored("Success - ", "green")
               FAILINFO = colored("Failure - ", "red")
               content = f.readlines()
               content_done = [x.strip() for x in content]
               split_content = random.choice(content_done)
               resp = req.get(split_content, headers=headers)
               arg()
               if resp.status_code == 200:
                  print("[INFO] " + INFO + split_content)
               else:
                  print("[INFO] " + FAILINFO + split_content)
            except:
               print("[INFO] An Error as ocurred, continuing..")
               pass


count = 0

def send_count():
       secs = int((time.time() - start_time))
       if secs < 60:
          super_type(f"\nSent {count} requests in " + "%s seconds." % (secs))
       if secs > 60:
          calc = secs / 60
          mins = int(calc)
          super_type(f"\nSent {count} requests in {mins} minutes.\n")
          print("")
       exit()

web_check()
file_check()
start_status()
start_time = time.time()

while True:
    try:
       count += 1
       if count == 100:
          end_count()
       if usr_input.lower() == "slow":
          text_stuff()
       if usr_input.lower() == "fast":
          text_stuff()
    except KeyboardInterrupt:
       send_count()
       time.sleep(2)
       os.system('cls' if os.name == 'nt' else 'clear')
